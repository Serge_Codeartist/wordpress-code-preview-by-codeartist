(function($){
	$(document).ready(function(){
		$('a').click(function(){
			if ($(this).prop('href').split('#')[0] == window.location.href.split('#')[0]) {
				var tag = $(this).attr('href').split('#')[1];
				if(tag) {
					if(tag == 'contact') {
						caOpenContactForm();
						return false;
					}
					else {
						var aTag = $('#' + tag);
						$('html,body').animate({scrollTop: aTag.offset().top},'slow');
						$('.ca_top_menu nav .ca_burger').trigger('click');
						return false;
					}
				}
			}
		});

		$('.ca_contact_form .container .ca_static a.ca_close_form').click(function(){
			caOpenContactForm();
			return false;
		})

		$('.ca_top_menu nav .ca_burger').click(function(){

			if($('.ca_top_menu nav').hasClass('ca_active'))
				$('.ca_top_menu nav').removeClass('ca_active');
			else
				$('.ca_top_menu nav').addClass('ca_active');

			return false;
		});

		var ca_current_measure = 0; //For the "Go down button"
		$('.ca_tech_simulation .ca_simulation .ca_scroll_down_button a').click(function(){
			var measure = (ca_current_measure < 0) ? 0 : ca_current_measure;
			measure = (measure > 6) ? 6 : measure;
			var el = $('.ca_screen_measure_' + (measure + 1) * 1);
			var scroll = el.offset().top + el.height() - $(window).height();
			if($(this).parent().hasClass('ca_btn_up')) scroll = $('.ca_tech_simulation').offset().top;
			$('html,body').animate({scrollTop: scroll},'slow');
			return false;
		});
		$('.ca_tech_video_placeholder .ca_arrow a').click(function(){
			var scroll = $('.ca_tech_simulation').offset().top;
			$('html,body').animate({scrollTop: scroll},'slow');
			return false;
		});
		$('.ca_tech_simulation .ca_pre_simulation .ca_arrow a').click(function(){
			var el = $('.ca_screen_measure_1');
			var scroll = el.offset().top + el.height() - $(window).height();
			$('html,body').animate({scrollTop: scroll},'slow');
			return false;
		});
		$.ca_animator([
			{
				element: 'body > section',
				boundaries: 'both',
				probe: 0,
				animations: [
					{
						action: function(params) {
							if(params.percent >= 50) {
								$('.ca_top_menu .ca_top_menu_bullets .row .col-12 ul li').removeClass('ca_active');
								$('.ca_top_menu .ca_top_menu_bullets .row .col-12 ul li').eq(params.element.index() - 1).addClass('ca_active');
							}
							if(params.percent > 50)
								if(params.element.get(0) == $('.ca_top_menu + section').get(0)) {
									$('.ca_top_menu').removeClass('ca_hidden');
								}
								else {
									if(params.direction == 'down') $('.ca_top_menu').addClass('ca_hidden');
									if(params.direction == 'up') $('.ca_top_menu').removeClass('ca_hidden');
								}
						},
					}
				]
			},
			{
				element: '.ca_home_vision_paralax_probe',
				boundaries: 'top',
				probe: 100,
				animations: [
					{
						action: function(params) {
							$('.ca_home_vision_paralax').css('top', -50 + (params.percentTop * 50 / 100) + "%");
						}
					}
				]
			},
			{
				element: '.ca_tech_simulation .ca_simulation .ca_screen_measure',
				boundaries: 'both',
				probe: 100,
				animations: [
					{
						action: function(params) {
							var measure = params.element.data('cameasure');
							//var left = params.percent > 0 ? 50 : -200;
							if(params.percent >= 50) ca_current_measure = measure;
							if((params.percent <= 0) && ($('.ca_screen_' + measure).hasClass('ca_shown'))) {
								$('.ca_screen_' + measure).removeClass('ca_shown');
							}
							if((params.percent > 0) && (!$('.ca_screen_' + measure).hasClass('ca_shown'))) {
								$('.ca_screen_' + measure).addClass('ca_shown');
							}
							$('.ca_screen_' + measure).css({
								//'left': left + '%',
								'opacity': (params.percent / 100),
							});
							$('.ca_screen_' + measure + ' svg .ca_el_group_2').css({
								'opacity': (params.percent / 100),
							});
						}
					}
				]
			},
			{
				element: '.ca_tech_simulation .ca_simulation .ca_screen_measure .ca_screen_part_2',
				boundaries: 'both',
				probe: 100,
				animations: [
					{
						action: function(params) {
							//console.log(params.percent);
							var measure = params.element.parent().data('cameasure');
							$('.ca_screen_' + measure + ' svg .ca_el_group_1').css({
								'opacity': (params.percent / 100),
							});
						}
					}
				]
			},
			{
				element: '.ca_tech_simulation .ca_simulation .ca_screen_measure .ca_screen_part_3',
				boundaries: 'both',
				probe: 100,
				animations: [
					{
						action: function(params) {
							//console.log(params.percent);
							var measure = params.element.parent().parent().data('cameasure');
							$('.ca_screen_' + measure + ' svg .ca_el_group_3').css({
								'opacity': (params.percent / 100),
							});
						}
					}
				]
			},
			{
				element: '.ca_tech_simulation .ca_simulation .ca_screen_measure_4',
				boundaries: 'both',
				probe: 100,
				animations: [
					{
						action: function(params) {
							var scale = 1 + (params.percentTop / 200);
							$('.ca_tech_simulation .ca_simulation .ca_screen_4').css({
								'transform': 'scale(' + scale + ', ' + scale + ')',
							});
							$('.ca_screen_4 svg.ca_group_fixed').css({
								'transform': 'scale(' + (1 - (params.percentTop / 400)) + ', ' + (1 - (params.percentTop / 400)) + ')',
							});
							$('.ca_screen_4').css({
								'transform': 'scale(' + scale + ', ' + scale + ')',
							});
						}
					}
				]
			},
			{
				element: '.ca_tech_simulation .ca_simulation .ca_screen_measure_6',
				boundaries: 'both',
				probe: 100,
				animations: [
					{
						action: function(params) {
							var scale = 1 + (params.percentTop / 200);
							$('.ca_screen_4 svg .ca_el_status, .ca_screen_4 svg .ca_el_landing, .ca_screen_4 svg .ca_el_dist').css({
								'opacity': (params.percent / 100),
							});
						}
					}
				]
			},
			{
				element: '.ca_simulation',
				boundaries: 'both',
				probe: 100,
				animations: [
					{
						action: function(params) {
							var display = (($(window).width() >= $(window).height()) ? 'none' : 'block');
							display = params.percent > 0 ? display : 'none';
							var button_display = params.percent > 0 ? 'block' : 'none';
							if(ca_current_measure == 0 && params.percent < 90) button_display = 'none';
							$('.ca_tech_simulation .ca_simulation .ca_rotate_placeholder').css({
								'opacity': (params.percent / 100),
								'display': display,
							});
							$('.ca_tech_simulation .ca_simulation .ca_scroll_down_button').css({
								'opacity': (params.percent / 100),
								'display': button_display,
							});
							if(ca_current_measure == 7) {
								if((!$('.ca_tech_simulation .ca_simulation .ca_scroll_down_button').hasClass('ca_btn_up')))
									$('.ca_tech_simulation .ca_simulation .ca_scroll_down_button').addClass('ca_btn_up');
							}
							else {
								if($('.ca_tech_simulation .ca_simulation .ca_scroll_down_button').hasClass('ca_btn_up'))
									$('.ca_tech_simulation .ca_simulation .ca_scroll_down_button').removeClass('ca_btn_up');
							}
						}
					}
				]
			},
			{
				element: '.ca_tech_simulation .ca_simulation .ca_screen_measure_6',
				boundaries: 'top',
				probe: 100,
				animations: [
					{
						action: function(params) {
							$('.ca_tech_simulation .ca_simulation .ca_scroll_down_button').css({
								'opacity': 1-(params.percent / 100),
							});
						}
					}
				]
			},
		]);

		$('.ca_slider_one').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			dots: false,
			appendArrows: '.ca_slider_one_nav',
			responsive: [
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 2,
					}
				}
			]
		});

		$('.ca_slider_two').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			dots: false,
			appendArrows: '.ca_slider_two_nav',
			responsive: [
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 1,
					}
				}
			]
		});

		function caOpenContactForm() {
			if($('.ca_contact_form').hasClass('ca_hidden'))
				$('.ca_contact_form').removeClass('ca_hidden');
			else
				$('.ca_contact_form').addClass('ca_hidden');

			return false;
		}
	});
})(jQuery);