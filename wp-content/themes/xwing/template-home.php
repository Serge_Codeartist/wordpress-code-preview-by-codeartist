<?php
/**
 * Template Name: Homepage Template
 *
 */
$opt = get_option("codeartist_options");
get_header(); ?>
	<section class="ca_home_top">
		<?php if($opt['ca_home_1_video']): ?>
			<div class="ca_video_container">
				<video autoplay muted loop>
					<source src="<?php echo $opt['ca_home_1_video']; ?>" type="video/mp4">
				</video>
			</div>
		<?php endif; ?>
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-6">
					<h1><?php echo $opt['ca_home_1_title']; ?></h1>
					<h2><?php echo $opt['ca_home_1_subtitle']; ?></h2>
					<p><?php echo $opt['ca_home_1_text']; ?></p>
					<div class="ca_arrow"></div>
				</div>
				<div class="col-md-5"></div>
			</div>
		</div>
	</section>
	<section class="ca_home_vision" id="vision">
		<div class="container">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<h2><?php echo $opt['ca_home_2_title']; ?></h2>
					<h3><?php echo $opt['ca_home_2_subtitle']; ?></h3>
					<p><?php echo $opt['ca_home_2_text']; ?></p>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="ca_home_vision_paralax" style="background-image: url(<?php echo $opt['ca_home_2_bg']; ?>) !important;"></div>
		<div class="ca_home_vision_paralax_probe"></div>
	</section>
	<section class="ca_home_techs">
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-4">
					<h2><?php echo $opt['ca_home_3_title']; ?></h2>
					<h3><?php echo $opt['ca_home_3_subtitle']; ?></h3>
					<p><?php echo $opt['ca_home_3_text']; ?></p>
					<a href="<?php echo $opt['ca_home_3_button_lnk']; ?>" class="ca_button"><?php echo $opt['ca_home_3_button_txt']; ?></a>
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-6">
					<div class="ca_tech_bubble">
						<div class="ca_num">01</div>
						<div class="img"><img src="<?php echo get_template_directory_uri().'/'; ?>img/ico_tech_1.svg" alt="<?php echo $opt['ca_home_3_1_name']; ?>"></div><h3><?php echo $opt['ca_home_3_1_name']; ?></h3>
						<a href="<?php echo $opt['ca_home_3_1_lnk']; ?>" class="ca_more">Learn More -</a>
						<p><?php echo $opt['ca_home_3_1_desc']; ?></p>
					</div>
					<div class="ca_tech_bubble">
						<div class="ca_num">02</div>
						<div class="img"><img src="<?php echo get_template_directory_uri().'/'; ?>img/ico_tech_2.svg" alt="<?php echo $opt['ca_home_3_2_name']; ?>"></div><h3><?php echo $opt['ca_home_3_2_name']; ?></h3>
						<a href="<?php echo $opt['ca_home_3_2_lnk']; ?>" class="ca_more">Learn More -</a>
						<p><?php echo $opt['ca_home_3_2_desc']; ?></p>
					</div>
					<div class="ca_tech_bubble">
						<div class="ca_num">03</div>
						<div class="img"><img src="<?php echo get_template_directory_uri().'/'; ?>img/ico_tech_3.svg" alt="<?php echo $opt['ca_home_3_3_name']; ?>"></div><h3><?php echo $opt['ca_home_3_3_name']; ?></h3>
						<a href="<?php echo $opt['ca_home_3_3_lnk']; ?>" class="ca_more">Learn More -</a>
						<p><?php echo $opt['ca_home_3_3_desc']; ?></p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="ca_home_company">
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-4">
					<h2><?php echo $opt['ca_home_4_title']; ?></h2>
					<h3><?php echo $opt['ca_home_4_subtitle']; ?></h3>
					<p><?php echo $opt['ca_home_4_text']; ?></p>
					<a href="<?php echo $opt['ca_home_4_button_lnk']; ?>" class="ca_button"><?php echo $opt['ca_home_4_button_txt']; ?></a>
				</div>
				<div class="col-md-7">
					<div class="img" style="background-image: url(<?php echo $opt['ca_home_4_img']; ?>);"></div>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>