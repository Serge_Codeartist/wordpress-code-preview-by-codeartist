<?php
/**
 * Header template for the theme
 *
 */
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title><?php
	// Print the <title> tag based on what is being viewed.
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
		echo esc_html( ' | ' . sprintf( __( 'Page %s', 'xwing' ), max( $paged, $page ) ) );

	?></title>
	<link rel='stylesheet' href='<?php echo get_template_directory_uri().'/'; ?>css/bootstrap.min.css' type='text/css' />
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
	<link rel='stylesheet' href='<?php echo get_template_directory_uri().'/'; ?>css/slick.css' type='text/css' />
	<link rel='stylesheet' href='<?php echo get_template_directory_uri().'/'; ?>css/style.css' type='text/css' />
	<script type='text/javascript' src='<?php echo get_template_directory_uri().'/'; ?>js/jquery-3.3.1.min.js'></script>
	<script type='text/javascript' src='<?php echo get_template_directory_uri().'/'; ?>js/bootstrap.min.js'></script>
	<script type='text/javascript' src='<?php echo get_template_directory_uri().'/'; ?>js/ca_animator.0.1.4.js'></script>
	<script type='text/javascript' src='<?php echo get_template_directory_uri().'/'; ?>js/slick.min.js'></script>
	<script type='text/javascript' src='<?php echo get_template_directory_uri().'/'; ?>js/script.js'></script>
	<?php wp_head(); ?>
</head>
<body>
	<header class="ca_top_menu">
		<div class="container">
			<div class="row">
				<div class="col-2">
					<a href="/" class="ca_logo"><img src="<?php echo get_template_directory_uri().'/'; ?>img/logo_h.svg" alt="XWing"></a>
				</div>
				<nav class="col-10">
					<a href="#" class="ca_burger">
						<span></span>
						<span></span>
						<span></span>
					</a>
					<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
				</nav>
			</div>
		</div>
		<?php
			if(is_page_template('template-home.php')) :
		?>
				<div class="container ca_top_menu_bullets">
					<div class="row">
						<div class="col-12">
							<ul>
								<li class="ca_active"><span class="ca_dots"></span><span>Overview</span></li>
								<li><span class="ca_dots"></span><span>Vision</span></li>
								<li><span class="ca_dots"></span><span>Technology</span></li>
								<li><span class="ca_dots"></span><span>Company</span></li>
							</ul>
						</div>
					</div>
				</div>
		<?php
			endif;
		?>
	</header>