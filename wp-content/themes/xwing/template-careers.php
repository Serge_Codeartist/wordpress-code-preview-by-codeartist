<?php
/**
 * Template Name: Careers Template
 *
 */
$opt = get_option("codeartist_options");
get_header(); ?>
	<section class="ca_home_top ca_careers_top">
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-5">
					<h1><?php echo $opt['ca_careers_1_title']; ?></h1>
					<?php echo $opt['ca_careers_1_text']; ?>
				</div>
				<div class="col-md-6"></div>
			</div>
		</div>
	</section>
	<section class="ca_careers_embeded">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-1"></div>
				<div class="col-12 col-md-10">
					<?php if($opt['ca_careers_2_title']): ?>
							<h2><?php echo $opt['ca_careers_2_title']; ?></h2>
					<?php endif; ?>
					<div class="ca_embedded_holder"><?php echo $opt['ca_careers_1_embed']; ?></div>
				</div>
				<div class="col-12 col-md-1"></div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>