<?php
	add_action( 'customize_register', 'codeartist_customize_register' );
	function codeartist_customize_register($wp_customize) {

		/*Регистрируем панели и секции*/

		$wp_customize->add_panel( 'ca_general', array(
			'priority'       => 70,
			'title'          => __('General Info', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'capability'     => 'edit_theme_options',
		));
		$wp_customize->add_section( 'ca_general_form', array(
			'priority'       => 72,
			'title'          => __('Contact Form', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'panel'  => 'ca_general',
		));
		$wp_customize->add_section( 'ca_general_general', array(
			'priority'       => 73,
			'title'          => __('General', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'panel'  => 'ca_general',
		));
		$wp_customize->add_section( 'ca_general_social', array(
			'priority'       => 74,
			'title'          => __('Social', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'panel'  => 'ca_general',
		));
		$wp_customize->add_panel( 'ca_home', array(
			'priority'       => 74,
			'title'          => __('Home Page', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'capability'     => 'edit_theme_options',
		));
		$wp_customize->add_section( 'ca_home_1', array(
			'priority'       => 76,
			'title'          => __('First Screen', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'panel'  => 'ca_home',
		));
		$wp_customize->add_section( 'ca_home_2', array(
			'priority'       => 77,
			'title'          => __('Second Screen', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'panel'  => 'ca_home',
		));
		$wp_customize->add_section( 'ca_home_3', array(
			'priority'       => 78,
			'title'          => __('Third Screen', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'panel'  => 'ca_home',
		));
		$wp_customize->add_section( 'ca_home_4', array(
			'priority'       => 79,
			'title'          => __('Fourth Screen', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'panel'  => 'ca_home',
		));
		$wp_customize->add_panel( 'ca_tech', array(
			'priority'       => 79,
			'title'          => __('Technology Page', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'capability'     => 'edit_theme_options',
		));
		$wp_customize->add_section( 'ca_tech_1', array(
			'priority'       => 81,
			'title'          => __('First Screen', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'panel'  => 'ca_tech',
		));
		$wp_customize->add_section( 'ca_tech_2', array(
			'priority'       => 82,
			'title'          => __('Second Screen', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'panel'  => 'ca_tech',
		));
		$wp_customize->add_section( 'ca_tech_3', array(
			'priority'       => 83,
			'title'          => __('Third Screen', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'panel'  => 'ca_tech',
		));
		$wp_customize->add_section( 'ca_tech_4', array(
			'priority'       => 84,
			'title'          => __('Fourth Screen', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'panel'  => 'ca_tech',
		));
		$wp_customize->add_panel( 'ca_company', array(
			'priority'       => 84,
			'title'          => __('Company Page', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'capability'     => 'edit_theme_options',
		));
		$wp_customize->add_section( 'ca_company_1', array(
			'priority'       => 86,
			'title'          => __('First screen', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'panel'  => 'ca_company',
		));
		$wp_customize->add_section( 'ca_company_2', array(
			'priority'       => 87,
			'title'          => __('Second screen', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'panel'  => 'ca_company',
		));
		$wp_customize->add_section( 'ca_company_3', array(
			'priority'       => 88,
			'title'          => __('Third screen', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'panel'  => 'ca_company',
		));
		$wp_customize->add_section( 'ca_company_4', array(
			'priority'       => 89,
			'title'          => __('Fourth Screen', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'panel'  => 'ca_company',
		));
		$wp_customize->add_panel( 'ca_careers', array(
			'priority'       => 89,
			'title'          => __('Careers Page', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'capability'     => 'edit_theme_options',
		));
		$wp_customize->add_section( 'ca_careers_1', array(
			'priority'       => 91,
			'title'          => __('First Screen', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'panel'  => 'ca_careers',
		));
		$wp_customize->add_section( 'ca_careers_2', array(
			'priority'       => 92,
			'title'          => __('Second Screen', 'codeartist'),
			'description'          => __('', 'codeartist'),
			'panel'  => 'ca_careers',
		));

		/*Добавляем расширения класса кастомайзера если требуется*/

		if( class_exists( 'WP_Customize_Control' ) ) {
			class WP_Customize_CA_group_Control extends WP_Customize_Control {
				public $type = 'ca_group';
				public function render_content() {
					echo'<span>'. esc_html( $this->label ) .'</span><span class="toggle-indicator" aria-hidden="true"></span>';
					echo'<style>
						#customize-control-'.$this->id.' {
							min-height: 3em;
							background: #fff;
							padding: 5px;
							padding-right: 30px;
							margin: 0;
							padding-bottom: 12px;
							box-sizing: border-box;
							border-bottom: solid 1px #ccc;
							position: relative;
							cursor: pointer;
							font-weight: bold;
						}
						#customize-control-'.$this->id.' span.toggle-indicator {
							position: absolute;
							top: 5px;
							right: 5px;
							display: block;
							width: 1em;
							height: 1em;
							border: solid 1px #72777C;
							border-radius: 1em;
						}
						#customize-control-'.$this->id.' span.toggle-indicator:before {
							content: "\f140";
							font: 400 18px/1 dashicons;
						}
						.ca_toggle_group_'.$this->id.' {
							margin: 0;
							background: #f7f7f7;
							box-sizing: border-box;
							padding: 5px;
							padding-bottom: 12px;
							border-left: solid 1px #ccc;
						}
						.ca_toggle_group_last {
							margin-bottom: 12px;
						}
					</style>
					<script>
						(function($){
							$(document).ready(function(){
								$("#customize-control-'.$this->id.'").nextAll().slice(0, '.$this->input_attrs["group_count"].').addClass("ca_toggle_group_'.$this->id.' ca_closed_group");
								$("#customize-control-'.$this->id.'").nextAll().slice('.($this->input_attrs["group_count"] - 1).', '.$this->input_attrs["group_count"].').addClass("ca_toggle_group_last");
								$(".ca_toggle_group_'.$this->id.'").hide();
								$("#customize-control-'.$this->id.'").click(function(){
									if($(".ca_toggle_group_'.$this->id.'").hasClass("ca_closed_group") == true) {
										$(".ca_toggle_group_'.$this->id.'").fadeIn();
										$(".ca_toggle_group_'.$this->id.'").removeClass("ca_closed_group");
									}
									else {
										$(".ca_toggle_group_'.$this->id.'").fadeOut();
										$(".ca_toggle_group_'.$this->id.'").addClass("ca_closed_group");
									}
								})
							});
						})(jQuery);
					</script>';
				}
			}
		}
		if( class_exists( 'WP_Customize_Control' ) ) {
			class WP_Customize_CA_link_Control extends WP_Customize_Control {
				public $type = 'ca_link';
				public function render_content() {
				echo'<label>
						<span class="customize-control-title">'. esc_html( $this->label ) .'</span>
						<span class="description customize-control-description">'. esc_html( $this->description ) .'</span>
						<a href="'. esc_html( $this->input_attrs["link_url"] ) .'" target="_blank" class="button control-focus">'. esc_html( $this->input_attrs["link_name"] ) .'</a><br>
					</label>';
				}
			}
		}
		if( class_exists( 'WP_Customize_Control' ) ) {
			class WP_Customize_CA_richtextarea_Control extends WP_Customize_Control {
				public $type = 'ca_richtextarea';
				public function render_content() {
					$ca_mediabuttons = esc_html( $this->input_attrs["media_buttons"] ) == 'true' ? 'true' : 'false';
					$ca_wpautop = esc_html( $this->input_attrs["wpautop"] ) == 'true' ? 'true' : 'false';
					echo '<label>
						<span class="customize-control-title">'.esc_html( $this->label ).'</span>
						<span class="description customize-control-description">'. esc_html( $this->description ) .'</span>';
						$settings = array(
							'media_buttons' => '.$ca_mediabuttons.',
							'quicktags' => true,
							'wpautop' => '.$ca_wpautop.',
						);
						$this->filter_editor_setting_link();
						wp_editor($this->value(), $this->id, $settings );
					echo '</label>
					<style>
						.mce-panel, .mce-tooltip, #wp-link-backdrop, .wp-core-ui, .media-modal-backdrop {
							z-index: 500000 !important;
						}
						.media-modal, .media-modal-close {
							z-index: 500001 !important;
						}
					</style>';
				}
				private function filter_editor_setting_link() {
					add_filter( 'the_editor', function( $output ) { return preg_replace( '/<textarea/', '<textarea ' . $this->get_link(), $output, 1 ); } );
				}
			}
		}

		/*Добавляем опции и контролы*/


		$wp_customize->add_setting( 'codeartist_options[ca_general_form_txt_1]' , array(
			'default' => __('Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_general_form_txt_1', array(
			'settings' => 'codeartist_options[ca_general_form_txt_1]',
			'label'    => __( 'Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_general_form',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_general_form_txt_2]' , array(
			'default' => __('Sub title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_general_form_txt_2', array(
			'settings' => 'codeartist_options[ca_general_form_txt_2]',
			'label'    => __( 'Sub title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_general_form',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_general_address]' , array(
			'default' => __('Address', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_general_address', array(
			'settings' => 'codeartist_options[ca_general_address]',
			'label'    => __( 'Address', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_general_general',
			'type'     => 'textarea',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_general_phone]' , array(
			'default' => __('Phone', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_general_phone', array(
			'settings' => 'codeartist_options[ca_general_phone]',
			'label'    => __( 'Phone', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_general_general',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_general_tw]' , array(
			'default' => __('Twitter link', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_general_tw', array(
			'settings' => 'codeartist_options[ca_general_tw]',
			'label'    => __( 'Twitter link', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_general_social',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_general_in]' , array(
			'default' => __('Instagram link', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_general_in', array(
			'settings' => 'codeartist_options[ca_general_in]',
			'label'    => __( 'Instagram link', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_general_social',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_home_1_title]' , array(
			'default' => __('Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_1_title', array(
			'settings' => 'codeartist_options[ca_home_1_title]',
			'label'    => __( 'Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_1',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_home_1_subtitle]' , array(
			'default' => __('Sub Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_1_subtitle', array(
			'settings' => 'codeartist_options[ca_home_1_subtitle]',
			'label'    => __( 'Sub Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_1',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_home_1_text]' , array(
			'default' => __('Text', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_1_text', array(
			'settings' => 'codeartist_options[ca_home_1_text]',
			'label'    => __( 'Text', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_1',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_home_1_video]' , array(
				'default' => __('Video Background', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Upload_Control( $wp_customize, 'codeartist_ca_home_1_video',
					array(
						'label'      => __( 'Video Background', 'codeartist' ),
						'description'	=> __('Must be MP4 file', 'codeartist'),
						'section'    => 'ca_home_1',
						'settings'   => 'codeartist_options[ca_home_1_video]',
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_home_2_title]' , array(
			'default' => __('Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_2_title', array(
			'settings' => 'codeartist_options[ca_home_2_title]',
			'label'    => __( 'Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_2',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_home_2_subtitle]' , array(
			'default' => __('Sub Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_2_subtitle', array(
			'settings' => 'codeartist_options[ca_home_2_subtitle]',
			'label'    => __( 'Sub Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_2',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_home_2_text]' , array(
			'default' => __('Text', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_2_text', array(
			'settings' => 'codeartist_options[ca_home_2_text]',
			'label'    => __( 'Text', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_2',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_home_2_bg]' , array(
				'default' => __('Background', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'codeartist_ca_home_2_bg',
					array(
						'label'      => __( 'Background', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_home_2',
						'settings'   => 'codeartist_options[ca_home_2_bg]',
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_home_3_title]' , array(
			'default' => __('Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_3_title', array(
			'settings' => 'codeartist_options[ca_home_3_title]',
			'label'    => __( 'Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_3',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_home_3_subtitle]' , array(
			'default' => __('Sub Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_3_subtitle', array(
			'settings' => 'codeartist_options[ca_home_3_subtitle]',
			'label'    => __( 'Sub Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_3',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_home_3_text]' , array(
			'default' => __('Text', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_3_text', array(
			'settings' => 'codeartist_options[ca_home_3_text]',
			'label'    => __( 'Text', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_3',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_home_3_button]' , array(
				'default' => __('Button', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_CA_group_Control( $wp_customize, 'codeartist_ca_home_3_button',
					array(
						'label'      => __( 'Button', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_home_3',
						'settings'   => 'codeartist_options[ca_home_3_button]',
						'input_attrs' => array(
							'group_count' => '2',
						),
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_home_3_button_txt]' , array(
			'default' => __('Text', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_3_button_txt', array(
			'settings' => 'codeartist_options[ca_home_3_button_txt]',
			'label'    => __( 'Text', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_3',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_home_3_button_lnk]' , array(
			'default' => __('Link', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_3_button_lnk', array(
			'settings' => 'codeartist_options[ca_home_3_button_lnk]',
			'label'    => __( 'Link', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_3',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_home_3_1]' , array(
				'default' => __('Technology 1', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_CA_group_Control( $wp_customize, 'codeartist_ca_home_3_1',
					array(
						'label'      => __( 'Technology 1', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_home_3',
						'settings'   => 'codeartist_options[ca_home_3_1]',
						'input_attrs' => array(
							'group_count' => '3',
						),
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_home_3_1_name]' , array(
			'default' => __('Name', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_3_1_name', array(
			'settings' => 'codeartist_options[ca_home_3_1_name]',
			'label'    => __( 'Name', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_3',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_home_3_1_desc]' , array(
			'default' => __('Description', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_3_1_desc', array(
			'settings' => 'codeartist_options[ca_home_3_1_desc]',
			'label'    => __( 'Description', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_3',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_home_3_1_lnk]' , array(
			'default' => __('Link', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_3_1_lnk', array(
			'settings' => 'codeartist_options[ca_home_3_1_lnk]',
			'label'    => __( 'Link', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_3',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_home_3_2]' , array(
				'default' => __('Technology 2', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_CA_group_Control( $wp_customize, 'codeartist_ca_home_3_2',
					array(
						'label'      => __( 'Technology 2', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_home_3',
						'settings'   => 'codeartist_options[ca_home_3_2]',
						'input_attrs' => array(
							'group_count' => '3',
						),
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_home_3_2_name]' , array(
			'default' => __('Name', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_3_2_name', array(
			'settings' => 'codeartist_options[ca_home_3_2_name]',
			'label'    => __( 'Name', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_3',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_home_3_2_desc]' , array(
			'default' => __('Description', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_3_2_desc', array(
			'settings' => 'codeartist_options[ca_home_3_2_desc]',
			'label'    => __( 'Description', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_3',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_home_3_2_lnk]' , array(
			'default' => __('Link', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_3_2_lnk', array(
			'settings' => 'codeartist_options[ca_home_3_2_lnk]',
			'label'    => __( 'Link', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_3',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_home_3_3]' , array(
				'default' => __('Technology 3', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_CA_group_Control( $wp_customize, 'codeartist_ca_home_3_3',
					array(
						'label'      => __( 'Technology 3', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_home_3',
						'settings'   => 'codeartist_options[ca_home_3_3]',
						'input_attrs' => array(
							'group_count' => '3',
						),
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_home_3_3_name]' , array(
			'default' => __('Name', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_3_3_name', array(
			'settings' => 'codeartist_options[ca_home_3_3_name]',
			'label'    => __( 'Name', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_3',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_home_3_3_desc]' , array(
			'default' => __('Description', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_3_3_desc', array(
			'settings' => 'codeartist_options[ca_home_3_3_desc]',
			'label'    => __( 'Description', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_3',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_home_3_3_lnk]' , array(
			'default' => __('Link', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_3_3_lnk', array(
			'settings' => 'codeartist_options[ca_home_3_3_lnk]',
			'label'    => __( 'Link', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_3',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_home_4_title]' , array(
			'default' => __('Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_4_title', array(
			'settings' => 'codeartist_options[ca_home_4_title]',
			'label'    => __( 'Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_4',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_home_4_subtitle]' , array(
			'default' => __('Sub Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_4_subtitle', array(
			'settings' => 'codeartist_options[ca_home_4_subtitle]',
			'label'    => __( 'Sub Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_4',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_home_4_text]' , array(
			'default' => __('Text', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_4_text', array(
			'settings' => 'codeartist_options[ca_home_4_text]',
			'label'    => __( 'Text', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_4',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_home_4_img]' , array(
				'default' => __('Image', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'codeartist_ca_home_4_img',
					array(
						'label'      => __( 'Image', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_home_4',
						'settings'   => 'codeartist_options[ca_home_4_img]',
					)
			));
			$wp_customize->add_setting( 'codeartist_options[ca_home_4_button]' , array(
				'default' => __('Button', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_CA_group_Control( $wp_customize, 'codeartist_ca_home_4_button',
					array(
						'label'      => __( 'Button', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_home_4',
						'settings'   => 'codeartist_options[ca_home_4_button]',
						'input_attrs' => array(
							'group_count' => '2',
						),
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_home_4_button_txt]' , array(
			'default' => __('Text', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_4_button_txt', array(
			'settings' => 'codeartist_options[ca_home_4_button_txt]',
			'label'    => __( 'Text', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_4',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_home_4_button_lnk]' , array(
			'default' => __('Link', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_home_4_button_lnk', array(
			'settings' => 'codeartist_options[ca_home_4_button_lnk]',
			'label'    => __( 'Link', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_home_4',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_tech_1_title]' , array(
			'default' => __('Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_1_title', array(
			'settings' => 'codeartist_options[ca_tech_1_title]',
			'label'    => __( 'Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_1',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_tech_1_subtitle]' , array(
			'default' => __('Sub Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_1_subtitle', array(
			'settings' => 'codeartist_options[ca_tech_1_subtitle]',
			'label'    => __( 'Sub Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_1',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_tech_1_text]' , array(
				'default' => __('Text', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_CA_richtextarea_Control( $wp_customize, 'codeartist_ca_tech_1_text',
					array(
						'label'      => __( 'Text', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_tech_1',
						'settings'   => 'codeartist_options[ca_tech_1_text]',
						'input_attrs' => array(
							'media_buttons' => '',
							'wpautop' => ''
						),
					)
			));
			$wp_customize->add_setting( 'codeartist_options[ca_tech_1_vid]' , array(
				'default' => __('Video', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Upload_Control( $wp_customize, 'codeartist_ca_tech_1_vid',
					array(
						'label'      => __( 'Video', 'codeartist' ),
						'description'	=> __('Must be MP4', 'codeartist'),
						'section'    => 'ca_tech_1',
						'settings'   => 'codeartist_options[ca_tech_1_vid]',
					)
			));
			$wp_customize->add_setting( 'codeartist_options[ca_tech_1_bg]' , array(
				'default' => __('Background', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'codeartist_ca_tech_1_bg',
					array(
						'label'      => __( 'Background', 'codeartist' ),
						'description'	=> __('If the video does not set, outputs this background', 'codeartist'),
						'section'    => 'ca_tech_1',
						'settings'   => 'codeartist_options[ca_tech_1_bg]',
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_1_header]' , array(
			'default' => __('Header', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_1_header', array(
			'settings' => 'codeartist_options[ca_tech_2_1_header]',
			'label'    => __( 'Header', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_tech_2_1]' , array(
				'default' => __('First Technology', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_CA_group_Control( $wp_customize, 'codeartist_ca_tech_2_1',
					array(
						'label'      => __( 'First Technology', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_tech_2',
						'settings'   => 'codeartist_options[ca_tech_2_1]',
						'input_attrs' => array(
							'group_count' => '13',
						),
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_1_title]' , array(
			'default' => __('Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_1_title', array(
			'settings' => 'codeartist_options[ca_tech_2_1_title]',
			'label'    => __( 'Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_1_subtitle]' , array(
			'default' => __('Sub Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_1_subtitle', array(
			'settings' => 'codeartist_options[ca_tech_2_1_subtitle]',
			'label'    => __( 'Sub Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_1_text]' , array(
			'default' => __('Text', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_1_text', array(
			'settings' => 'codeartist_options[ca_tech_2_1_text]',
			'label'    => __( 'Text', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_1_item_1]' , array(
			'default' => __('Item 1', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_1_item_1', array(
			'settings' => 'codeartist_options[ca_tech_2_1_item_1]',
			'label'    => __( 'Item 1', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_tech_2_1_icon_1]' , array(
				'default' => __('Icon 1', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'codeartist_ca_tech_2_1_icon_1',
					array(
						'label'      => __( 'Icon 1', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_tech_2',
						'settings'   => 'codeartist_options[ca_tech_2_1_icon_1]',
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_1_item_2]' , array(
			'default' => __('Item 2', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_1_item_2', array(
			'settings' => 'codeartist_options[ca_tech_2_1_item_2]',
			'label'    => __( 'Item 2', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_tech_2_1_icon_2]' , array(
				'default' => __('Icon 2', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'codeartist_ca_tech_2_1_icon_2',
					array(
						'label'      => __( 'Icon 2', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_tech_2',
						'settings'   => 'codeartist_options[ca_tech_2_1_icon_2]',
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_1_item_3]' , array(
			'default' => __('Item 3', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_1_item_3', array(
			'settings' => 'codeartist_options[ca_tech_2_1_item_3]',
			'label'    => __( 'Item 3', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_tech_2_1_icon_3]' , array(
				'default' => __('Icon 3', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'codeartist_ca_tech_2_1_icon_3',
					array(
						'label'      => __( 'Icon 3', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_tech_2',
						'settings'   => 'codeartist_options[ca_tech_2_1_icon_3]',
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_1_item_4]' , array(
			'default' => __('Item 4', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_1_item_4', array(
			'settings' => 'codeartist_options[ca_tech_2_1_item_4]',
			'label'    => __( 'Item 4', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_tech_2_1_icon_4]' , array(
				'default' => __('Icon 4', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'codeartist_ca_tech_2_1_icon_4',
					array(
						'label'      => __( 'Icon 4', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_tech_2',
						'settings'   => 'codeartist_options[ca_tech_2_1_icon_4]',
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_1_item_5]' , array(
			'default' => __('Item 5', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_1_item_5', array(
			'settings' => 'codeartist_options[ca_tech_2_1_item_5]',
			'label'    => __( 'Item 5', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_tech_2_1_icon_5]' , array(
				'default' => __('Icon 5', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'codeartist_ca_tech_2_1_icon_5',
					array(
						'label'      => __( 'Icon 5', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_tech_2',
						'settings'   => 'codeartist_options[ca_tech_2_1_icon_5]',
					)
			));
			$wp_customize->add_setting( 'codeartist_options[ca_tech_2_2]' , array(
				'default' => __('Second Technology', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_CA_group_Control( $wp_customize, 'codeartist_ca_tech_2_2',
					array(
						'label'      => __( 'Second Technology', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_tech_2',
						'settings'   => 'codeartist_options[ca_tech_2_2]',
						'input_attrs' => array(
							'group_count' => '15',
						),
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_2_title]' , array(
			'default' => __('Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_2_title', array(
			'settings' => 'codeartist_options[ca_tech_2_2_title]',
			'label'    => __( 'Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_2_subtitle]' , array(
			'default' => __('Sub Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_2_subtitle', array(
			'settings' => 'codeartist_options[ca_tech_2_2_subtitle]',
			'label'    => __( 'Sub Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_2_text]' , array(
			'default' => __('Text', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_2_text', array(
			'settings' => 'codeartist_options[ca_tech_2_2_text]',
			'label'    => __( 'Text', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_2_item_1]' , array(
			'default' => __('Item 1', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_2_item_1', array(
			'settings' => 'codeartist_options[ca_tech_2_2_item_1]',
			'label'    => __( 'Item 1', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_tech_2_2_icon_1]' , array(
				'default' => __('Icon 1', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'codeartist_ca_tech_2_2_icon_1',
					array(
						'label'      => __( 'Icon 1', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_tech_2',
						'settings'   => 'codeartist_options[ca_tech_2_2_icon_1]',
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_2_item_2]' , array(
			'default' => __('Item 2', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_2_item_2', array(
			'settings' => 'codeartist_options[ca_tech_2_2_item_2]',
			'label'    => __( 'Item 2', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_tech_2_2_icon_2]' , array(
				'default' => __('Icon 2', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'codeartist_ca_tech_2_2_icon_2',
					array(
						'label'      => __( 'Icon 2', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_tech_2',
						'settings'   => 'codeartist_options[ca_tech_2_2_icon_2]',
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_2_item_3]' , array(
			'default' => __('Item 3', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_2_item_3', array(
			'settings' => 'codeartist_options[ca_tech_2_2_item_3]',
			'label'    => __( 'Item 3', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_tech_2_2_icon_3]' , array(
				'default' => __('Icon 3', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'codeartist_ca_tech_2_2_icon_3',
					array(
						'label'      => __( 'Icon 3', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_tech_2',
						'settings'   => 'codeartist_options[ca_tech_2_2_icon_3]',
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_2_item_4]' , array(
			'default' => __('Item 4', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_2_item_4', array(
			'settings' => 'codeartist_options[ca_tech_2_2_item_4]',
			'label'    => __( 'Item 4', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_tech_2_2_icon_4]' , array(
				'default' => __('Icon 4', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'codeartist_ca_tech_2_2_icon_4',
					array(
						'label'      => __( 'Icon 4', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_tech_2',
						'settings'   => 'codeartist_options[ca_tech_2_2_icon_4]',
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_2_item_5]' , array(
			'default' => __('Item 5', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_2_item_5', array(
			'settings' => 'codeartist_options[ca_tech_2_2_item_5]',
			'label'    => __( 'Item 5', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_tech_2_2_icon_5]' , array(
				'default' => __('Icon 5', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'codeartist_ca_tech_2_2_icon_5',
					array(
						'label'      => __( 'Icon 5', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_tech_2',
						'settings'   => 'codeartist_options[ca_tech_2_2_icon_5]',
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_2_item_6]' , array(
			'default' => __('Item 6', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_2_item_6', array(
			'settings' => 'codeartist_options[ca_tech_2_2_item_6]',
			'label'    => __( 'Item 6', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_tech_2_2_icon_6]' , array(
				'default' => __('Icon 6', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'codeartist_ca_tech_2_2_icon_6',
					array(
						'label'      => __( 'Icon 6', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_tech_2',
						'settings'   => 'codeartist_options[ca_tech_2_2_icon_6]',
					)
			));
			$wp_customize->add_setting( 'codeartist_options[ca_tech_2_3]' , array(
				'default' => __('Third Technology', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_CA_group_Control( $wp_customize, 'codeartist_ca_tech_2_3',
					array(
						'label'      => __( 'Third Technology', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_tech_2',
						'settings'   => 'codeartist_options[ca_tech_2_3]',
						'input_attrs' => array(
							'group_count' => '11',
						),
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_3_title]' , array(
			'default' => __('Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_3_title', array(
			'settings' => 'codeartist_options[ca_tech_2_3_title]',
			'label'    => __( 'Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_3_subtitle]' , array(
			'default' => __('Sub Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_3_subtitle', array(
			'settings' => 'codeartist_options[ca_tech_2_3_subtitle]',
			'label'    => __( 'Sub Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_3_text]' , array(
			'default' => __('Text', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_3_text', array(
			'settings' => 'codeartist_options[ca_tech_2_3_text]',
			'label'    => __( 'Text', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_3_item_1]' , array(
			'default' => __('Item 1', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_3_item_1', array(
			'settings' => 'codeartist_options[ca_tech_2_3_item_1]',
			'label'    => __( 'Item 1', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_tech_2_3_icon_1]' , array(
				'default' => __('Icon 1', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'codeartist_ca_tech_2_3_icon_1',
					array(
						'label'      => __( 'Icon 1', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_tech_2',
						'settings'   => 'codeartist_options[ca_tech_2_3_icon_1]',
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_3_item_2]' , array(
			'default' => __('Item 2', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_3_item_2', array(
			'settings' => 'codeartist_options[ca_tech_2_3_item_2]',
			'label'    => __( 'Item 2', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_tech_2_3_icon_2]' , array(
				'default' => __('Icon 2', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'codeartist_ca_tech_2_3_icon_2',
					array(
						'label'      => __( 'Icon 2', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_tech_2',
						'settings'   => 'codeartist_options[ca_tech_2_3_icon_2]',
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_3_item_3]' , array(
			'default' => __('Item 3', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_3_item_3', array(
			'settings' => 'codeartist_options[ca_tech_2_3_item_3]',
			'label'    => __( 'Item 3', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_tech_2_3_icon_3]' , array(
				'default' => __('Icon 3', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'codeartist_ca_tech_2_3_icon_3',
					array(
						'label'      => __( 'Icon 3', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_tech_2',
						'settings'   => 'codeartist_options[ca_tech_2_3_icon_3]',
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_tech_2_3_item_4]' , array(
			'default' => __('Item 4', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_2_3_item_4', array(
			'settings' => 'codeartist_options[ca_tech_2_3_item_4]',
			'label'    => __( 'Item 4', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_2',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_tech_2_3_icon_4]' , array(
				'default' => __('Icon 4', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'codeartist_ca_tech_2_3_icon_4',
					array(
						'label'      => __( 'Icon 4', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_tech_2',
						'settings'   => 'codeartist_options[ca_tech_2_3_icon_4]',
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_tech_3_title]' , array(
			'default' => __('Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_3_title', array(
			'settings' => 'codeartist_options[ca_tech_3_title]',
			'label'    => __( 'Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_3',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_tech_3_subtitle]' , array(
			'default' => __('Sub Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_3_subtitle', array(
			'settings' => 'codeartist_options[ca_tech_3_subtitle]',
			'label'    => __( 'Sub Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_3',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_tech_4_title]' , array(
			'default' => __('Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_4_title', array(
			'settings' => 'codeartist_options[ca_tech_4_title]',
			'label'    => __( 'Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_4',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_tech_4_subtitle]' , array(
			'default' => __('Sub Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_4_subtitle', array(
			'settings' => 'codeartist_options[ca_tech_4_subtitle]',
			'label'    => __( 'Sub Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_tech_4',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_tech_4_video]' , array(
			'default' => __('Video embed code', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_tech_4_video', array(
			'settings' => 'codeartist_options[ca_tech_4_video]',
			'label'    => __( 'Video embed code', 'codeartist' ),
			'description'    => __( 'Youtube or Vimeo iFrame code', 'codeartist' ),
			'section'  => 'ca_tech_4',
			'type'     => 'textarea',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_company_1_title]' , array(
			'default' => __('Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_company_1_title', array(
			'settings' => 'codeartist_options[ca_company_1_title]',
			'label'    => __( 'Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_company_1',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_company_2_title]' , array(
			'default' => __('Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_company_2_title', array(
			'settings' => 'codeartist_options[ca_company_2_title]',
			'label'    => __( 'Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_company_2',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_company_2_txt_quote]' , array(
			'default' => __('Quote', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_company_2_txt_quote', array(
			'settings' => 'codeartist_options[ca_company_2_txt_quote]',
			'label'    => __( 'Quote', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_company_2',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_company_2_txt_quote_author]' , array(
			'default' => __('Quote author', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_company_2_txt_quote_author', array(
			'settings' => 'codeartist_options[ca_company_2_txt_quote_author]',
			'label'    => __( 'Quote author', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_company_2',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_company_2_txt]' , array(
				'default' => __('Text', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_CA_richtextarea_Control( $wp_customize, 'codeartist_ca_company_2_txt',
					array(
						'label'      => __( 'Text', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_company_2',
						'settings'   => 'codeartist_options[ca_company_2_txt]',
						'input_attrs' => array(
							'media_buttons' => '',
							'wpautop' => ''
						),
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_company_3_title]' , array(
			'default' => __('Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_company_3_title', array(
			'settings' => 'codeartist_options[ca_company_3_title]',
			'label'    => __( 'Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_company_3',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_company_3_members]' , array(
				'default' => __('Team', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_CA_link_Control( $wp_customize, 'codeartist_ca_company_3_members',
					array(
						'label'      => __( 'Team', 'codeartist' ),
						'description'	=> __('Edit members', 'codeartist'),
						'section'    => 'ca_company_3',
						'settings'   => 'codeartist_options[ca_company_3_members]',
						'input_attrs' => array(
							'link_name' => 'Open editor',
							'link_url' => '/wp-admin/edit.php?post_type=ca_team'
						),
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_company_4_title]' , array(
			'default' => __('Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_company_4_title', array(
			'settings' => 'codeartist_options[ca_company_4_title]',
			'label'    => __( 'Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_company_4',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_company_4_subtitle]' , array(
			'default' => __('Sub Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_company_4_subtitle', array(
			'settings' => 'codeartist_options[ca_company_4_subtitle]',
			'label'    => __( 'Sub Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_company_4',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_company_4_companies]' , array(
				'default' => __('Companies', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_CA_link_Control( $wp_customize, 'codeartist_ca_company_4_companies',
					array(
						'label'      => __( 'Companies', 'codeartist' ),
						'description'	=> __('Edit companies', 'codeartist'),
						'section'    => 'ca_company_4',
						'settings'   => 'codeartist_options[ca_company_4_companies]',
						'input_attrs' => array(
							'link_name' => 'Open editor',
							'link_url' => '/wp-admin/edit.php?post_type=ca_company'
						),
					)
			));
			$wp_customize->add_setting( 'codeartist_options[ca_company_4_investors]' , array(
				'default' => __('Investors', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_CA_link_Control( $wp_customize, 'codeartist_ca_company_4_investors',
					array(
						'label'      => __( 'Investors', 'codeartist' ),
						'description'	=> __('Edit investors', 'codeartist'),
						'section'    => 'ca_company_4',
						'settings'   => 'codeartist_options[ca_company_4_investors]',
						'input_attrs' => array(
							'link_name' => 'Open editor',
							'link_url' => '/wp-admin/edit.php?post_type=ca_investor'
						),
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_company_4_btn_txt]' , array(
			'default' => __('Link text', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_company_4_btn_txt', array(
			'settings' => 'codeartist_options[ca_company_4_btn_txt]',
			'label'    => __( 'Link text', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_company_4',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_company_4_btn_lnk]' , array(
			'default' => __('Link URL', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_company_4_btn_lnk', array(
			'settings' => 'codeartist_options[ca_company_4_btn_lnk]',
			'label'    => __( 'Link URL', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_company_4',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_careers_1_title]' , array(
			'default' => __('Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_careers_1_title', array(
			'settings' => 'codeartist_options[ca_careers_1_title]',
			'label'    => __( 'Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_careers_1',
			'type'     => 'text',
		));

			$wp_customize->add_setting( 'codeartist_options[ca_careers_1_text]' , array(
				'default' => __('Text', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_CA_richtextarea_Control( $wp_customize, 'codeartist_ca_careers_1_text',
					array(
						'label'      => __( 'Text', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_careers_1',
						'settings'   => 'codeartist_options[ca_careers_1_text]',
						'input_attrs' => array(
							'media_buttons' => '',
							'wpautop' => ''
						),
					)
			));
		$wp_customize->add_setting( 'codeartist_options[ca_careers_2_title]' , array(
			'default' => __('Title', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_careers_2_title', array(
			'settings' => 'codeartist_options[ca_careers_2_title]',
			'label'    => __( 'Title', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_careers_2',
			'type'     => 'text',
		));

		$wp_customize->add_setting( 'codeartist_options[ca_careers_1_embed]' , array(
			'default' => __('Embed code', 'codeartist'),
			'type' => 'option',
		));
		$wp_customize->add_control( 'codeartist_ca_careers_1_embed', array(
			'settings' => 'codeartist_options[ca_careers_1_embed]',
			'label'    => __( 'Embed code', 'codeartist' ),
			'description'    => __( '', 'codeartist' ),
			'section'  => 'ca_careers_2',
			'type'     => 'textarea',
		));

			if( class_exists( 'WP_Customize_Control' ) ) {
				class WP_Customize_CA_richtextareaHelper_Control extends WP_Customize_Control {
					public $type = 'ca_richtextareahelper';
					public function render_content() {
						do_action('admin_footer');
						do_action('admin_print_footer_scripts');
					}
				}
			}
			$wp_customize->add_setting( 'codeartist_helper' , array(
				'default' => __('Text', 'codeartist'),
				'type' => 'option',
			));
			$wp_customize->add_control( new WP_Customize_CA_richtextareaHelper_Control( $wp_customize, 'codeartist_helper',
					array(
						'label'      => __( 'Text', 'codeartist' ),
						'description'	=> __('', 'codeartist'),
						'section'    => 'ca_about_who',
						'settings'   => 'codeartist_helper',
					)
			));

	}

	add_action( 'customize_controls_print_scripts', 'ca_tinymce_customizer_script', 999 );
	function ca_tinymce_customizer_script() {
		wp_enqueue_editor();
		echo"<script>
				( function( $ ) {
					$(document).ready(function(){
						wp.editor.initialize();
						var editorSettings = wp.editor.getDefaultSettings();
						tinymce.init(editorSettings.tinymce);

						wp.customizerCtrlEditor = {

							init: function() {

								$(window).load(function(){

									$('textarea.wp-editor-area').each(function(){
										var tArea = $(this),
											id = tArea.attr('id'),
											editor = tinyMCE.get(id),
											setChange,
											content;
											tinyMCE.execCommand('mceAddEditor', true, id);
										var theInt = setInterval(function(){
											editor = tinyMCE.get(id);
											if(editor){
												editor.onChange.add(function (ed, e) {
													ed.save();
													content = editor.getContent();
													clearTimeout(setChange);
													setChange = setTimeout(function(){
														tArea.val(content).trigger('change');
													},500);
													console.log('changed');
												});
												clearInterval(theInt);
											}
										}, 500);

										tArea.css({
											visibility: 'visible'
										});
									});
								});
							}

						};

						wp.customizerCtrlEditor.init();
					});
				} )( jQuery );
			</script>";
	}
?>