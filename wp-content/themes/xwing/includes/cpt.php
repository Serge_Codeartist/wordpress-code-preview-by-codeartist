<?php

add_action('admin_menu', 'ca_xwing_admin');

function ca_xwing_admin() {
	add_menu_page("XWing", "XWing", "manage_options", 'xwing', "", "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c3ZnIHdpZHRoPSIxMDdweCIgaGVpZ2h0PSI4NnB4IiB2aWV3Qm94PSIwIDAgMTA3IDg2IiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPiAgICAgICAgPHRpdGxlPkUzNTUwRDQxLTI2RDItNDZDQi1BQjRCLTRDNjAzQ0YwRkRGNzwvdGl0bGU+ICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBza2V0Y2h0b29sLjwvZGVzYz4gICAgPGRlZnM+PC9kZWZzPiAgICA8ZyBpZD0iU3ltYm9scyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+ICAgICAgICA8ZyBpZD0iRm9vdGVyIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMjMxLjAwMDAwMCwgLTcyLjAwMDAwMCkiIGZpbGw9IiNGRkZGRkYiPiAgICAgICAgICAgIDxnIGlkPSJsb2dvIj4gICAgICAgICAgICAgICAgPGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMjMxLjAwMDAwMCwgNzIuMDAwMDAwKSI+ICAgICAgICAgICAgICAgICAgICA8ZyBpZD0iR3JvdXAtMiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoOS43MjcyNzMsIDAuMDAwMDAwKSI+ICAgICAgICAgICAgICAgICAgICAgICAgPHBhdGggZD0iTTc5LjIyNzI5NjIsMC4wNjYxMjY5NDc3IEM3Ny42OTU3NDEzLDAuMjg4MzA1MDY4IDc2LjE4MzE0MDEsMC41NjM2NTg1MjEgNzQuNjc3MzgzMSwwLjg1OTU0NTAyMiBDNzMuNzgzNDA1OCwxLjAzNTM5MjE2IDcyLjg5MzExMzgsMS4yMTk2NjMxMSA3Mi4wMDQ0MDEzLDEuNDA5MTk4OTUgQzcxLjExNjc0MTgsMS42MDM0NzMxOCA3MC4yMzI3Njc4LDEuODA1MTE4MjUgNjkuMzQ4NzkzNywyLjAwODM0Mjc5IEM2Ny41ODM0Nzc5LDIuNDIwMDU2NzUgNjUuODIxODQ3NiwyLjgzODYxNTA2IDY0LjA2OTE2NzYsMy4yODAzMzg4NiBDNTcuMDYxMDgsNS4wNDcyMzQwNSA1MC4xMjE0MzYsNi45OTk5Nzk2NiA0My4yMzQ5NjcyLDguODU5MDEwMzMgQzQxLjUxNTQ1Niw5LjMyNDQyNjExIDM5LjEwODg3NzMsMTAuMTEyMDUyOCAzOC4zNDEyNTcyLDEwLjMwMTA2MjIgQzM3LjU3MzYzNywxMC40OTA1OTggMzQuOTE5MDgyMywxMS4xOTA4Mjc2IDMzLjg3NjEwODcsMTEuMjgyOTYzMSBDMzIuODMzNjYxNiwxMS4zNzQ1NzIxIDMwLjk0NzI1MzYsMTEuMzAwMzM3MiAyOS42MDY4MTQsMTAuOTg4MTI5NiBDMjguMjU3NDI0MSwxMC42ODA2NjAzIDI2LjgzOTU5MDgsMTAuMTgyMDc1OCAyNS40MTg1OTg1LDkuNTY1NTU3ODIgQzIyLjU2ODcxNjYsOC4zMjUxNTEwNiAxOS43MjA0MTQxLDYuNjMwOTExMjcgMTYuOTQzNzE0MSw0Ljc4MDgzMDkgQzE2LjIyODc0MjgsNC4yOTQ4ODIwNyAxNS41MTQ4MjQ1LDMuNzk5NDU2NDUgMTQuODA2MTcxMSwzLjI5NDAyNzU1IEMxMC4yMjc4Mjc2LC0wLjAzNzU5MTI3NDQgMy4yOTA4MTU5OCwzLjQzODI4NTM5IDEuNDI3NTczNCw0LjQ4MjgzODQ1IEMxLjI2OTEwMDM4LDQuNTc0NDQ3NDMgMS4xMTQzMTI3OCw0LjY3OTc0NTEyIDEuMTA5MDQ3ODksNC45MTQ1NTg5NiBDMS4xMDg1MjE0LDUuMDUzMDI1NDIgMS4xNzE3MDAwMiw1LjE3NDExNzc2IDEuMTcyNzUyOTksNS4xNzYyMjM3MiBDMS40NDMzNjgwNSw1LjY5NDgxNDgzIDEuNzExODc3MTUsNS45NzMzMjcyMSAyLjE2OTkyMjA5LDYuMzg5MjUzMDggTDIuNDc2MzM4MzYsNi42NjcyMzg5NyBMMi43NTQ4NTA3NSw2LjkxNTc0MTUxIEwzLjMwNjA4NDE0LDcuMzkyNzQwMDQgQzMuNjcyNTIwMDksNy43MDcwNTM2MyA0LjAzODQyOTU2LDguMDE1NTc1ODYgNC40MDk2MDM5LDguMzE1MTQ3NzggQzUuMTUxNDI2MTEsOC45MTUzNDQ2IDUuODk0MzAxMyw5LjUwNzExNzYgNi42NTYxMzAwNiwxMC4wNzU3MjUxIEM4LjE3ODIwODE0LDExLjIxMzk5MzEgOS43MzYwODc0MiwxMi4zMDg1NjI2IDExLjM1NzE0NTMsMTMuMzMyNTgyNiBDMTIuMTY1MzA1MSwxMy44NDc0ODgzIDEyLjk4NjEwMDUsMTQuMzQ5NzU4MiAxMy44MjExMTEyLDE0LjgzNzI4NjUgQzE0LjY1OTI4MDgsMTUuMzE5NTQ5OSAxNS41MTE2NjU2LDE1Ljc4NjAxODcgMTYuMzgxOTUxLDE2LjIzNTYzOTggQzE4LjEyNDYyNzcsMTcuMTI4MDM3NyAxOS45MzUyMjE0LDE3Ljk1MDQxMjcgMjEuODUyMTY1OCwxOC42NDM3OTc5IEMyMi44MTAzNzQ4LDE4Ljk5Mjg1OTggMjMuNzk3MDE0MSwxOS4zMDUwNjc0IDI0LjgxNTI0MjcsMTkuNTc1MTU2IEMyNS44MzQ1MjQ0LDE5Ljg0MzY2NTEgMjYuODg2NDQ4MywyMC4wNzMyMTQgMjcuOTc5OTY0NywyMC4yMjgwMDE2IEMyOS4wNzM0ODEyLDIwLjM4MzMxNTcgMzAuMjA0OTA0OSwyMC40NzkxMzY2IDMxLjM4MjY1OTUsMjAuNDYyMjg5IEMzMi41NTgzMDgyLDIwLjQ0NDkxNDkgMzMuNzc2NjAyNCwyMC4zMTgwMzEyIDM1LjAwNDM3MzUsMjAuMDQwMDQ1MyBDMzUuMTU3NTgxNiwyMC4wMDU4MjM1IDM1LjMxMTg0MjcsMTkuOTYzMTc4IDM1LjQ2NTA1MDgsMTkuOTI0MjE3OCBMMzUuNjk0NTk5OCwxOS44NjM2NzE2IEwzNS44MDkzNzQzLDE5LjgzMzEzNTMgTDM1Ljg5ODM1MDgsMTkuODA2Mjg0NCBMMzYuNTg3NTI0MiwxOS41OTM1ODMxIEMzNy4wNDI5MzY3LDE5LjQ0OTMyNTIgMzcuNDkwOTc4NCwxOS4yOTk4MDI1IDM3LjkzNDI4MTYsMTkuMTQ5NzUzMyBDMzguODIxOTQxMSwxOC44NDg2MDE5IDM5LjY5Mjc1MywxOC41Mzk1NTMyIDQwLjU1NzI0NywxOC4yMjU3NjYxIEM0Mi4yODMwNzYxLDE3LjU5ODE5MTkgNDMuOTgzMTA3MywxNi45NTM3NzAxIDQ1LjY3MzEzNTEsMTYuMzAyNTAzOSBDNTIuNDIxNjYzOSwxMy42ODQ4MDMzIDU5LjAzNzUxNzYsMTAuOTI1NDc3NCA2NS42ODQ0MzQxLDguMjc2NzE0MTIgQzY3LjM0NjAzMTcsNy42MTQzOTE2NyA2OS4wMDM5NDM3LDYuOTQ3ODU3MzEgNzAuNjY3NjQ3Miw2LjMwNjA2NzkgQzcxLjQ5OTQ5ODksNS45ODQ5MDk5NiA3Mi4zMzE4NzcyLDUuNjY2Mzg0NDUgNzMuMTY1ODM0OCw1LjM1NDcwMzMgQzczLjk5ODIxMzEsNS4wMzg4MTAyMyA3NC44MzExMTc4LDQuNzI2MDc2MSA3NS42NjU2MDE5LDQuNDIyODE4NzYgQzc3LjMzNTYyMzMsMy44MTk5ODk1IDc5LjAxMTQzNiwzLjIzOTc5OTI1IDgwLjY4OTM1NDYsMi43MjE3MzQ2MiBDODEuNTI4NTc3MiwyLjQ2NDI4MTc4IDgyLjM3MTQ4NTIsMi4yMjk0Njc5NCA4My4yMDg2MDE4LDIuMDI2MjQzNCBDODQuMDQyNTU5NSwxLjgyNTEyNDgyIDg0Ljg4NjUyMDQsMS42NTk4MDc0NSA4NS42NzA0NjE3LDEuNjA0NTI2MTYgQzg1LjY3MDQ2MTcsMS42MDQ1MjYxNiA4NS45NDM3MDkyLDEuNjAxMzY3MjMgODYuMjAxNjg4NSwxLjQyMTgzNDY3IEM4My41MDAyNzY0LC0wLjEzNTUxODEyNCA4MC41NzI0NzQyLC0wLjA3MzkxODk3NjUgNzkuMjI3Mjk2MiwwLjA2NjEyNjk0NzciIGlkPSJGaWxsLTEiPjwvcGF0aD4gICAgICAgICAgICAgICAgICAgICAgICA8cGF0aCBkPSJNODYuNjMwMDM5NSw0NC41NjQ2NjY0IEM4Ni40MzEwMjY5LDQ0LjI3MzUxODMgODYuMjgzNjEwMSw0NC4xMTkyNTcyIDg2LjAwODI1NjcsNDMuODYxODA0MyBMODUuNDQwNzAyMSw0My4zMjg5OTggTDg0Ljg5MDUyMTcsNDIuODIzMDQyNiBDODQuNTI0MDg1OCw0Mi40ODgxOTYgODQuMTUxODU4NSw0Mi4xNjQ0MDU2IDgzLjc4MTczNzEsNDEuODM4NTA5MyBDODMuMDQyMDIwOCw0MS4xODY3MTY2IDgyLjI5MTc3NDgsNDAuNTUxNzcxNSA4MS41MzIwNTIsMzkuOTI3MzU2MiBDODAuMDE0NzEyMywzOC42Nzg1MjU3IDc4LjQ2NjgzNjMsMzcuNDY3NjAyMyA3Ni44ODI2MzI2LDM2LjI5NjE2NTUgQzczLjcxNzM4NDEsMzMuOTUwMTMzIDcwLjQyNjgzMTQsMzEuNzQzNjIgNjYuOTUzNTg3MiwyOS43MTY2Mzk1IEM2My40ODA4Njk0LDI3LjY4Mzg2NzYgNTkuODIwNzIxOCwyNS44NDIyMTExIDU1LjY3MDk0LDI0LjU3NzA1OTQgQzUzLjU5NTUyMjYsMjMuOTQwNTM0OSA1MS40MTI3MDE1LDIzLjQxMzUxOTkgNDguOTUxODk0NSwyMy4xNjUwMTc0IEM0Ny43MTY3NTI3LDIzLjAzOTcxMzEgNDYuMzk4OTUyMSwyMy4wMDc1OTczIDQ0Ljk1MzIxNDksMjMuMTUwODAyMiBDNDQuNTk5OTQxMSwyMy4xNzkyMzI2IDQ0LjIxNTA3ODEsMjMuMjQ4NzI5IDQzLjg0NjAwOTcsMjMuMzA2NjQyOCBDNDMuNzQzODcwOSwyMy4zMjEzODQ1IDQzLjY4MjI3MTgsMjMuMzM4NzU4NiA0My42MTAxNDI5LDIzLjM1NDU1MzIgTDQzLjM5Nzk2OCwyMy40MDM1MTY2IEw0Mi45NzQ2NzEzLDIzLjUwMTQ0MzUgTDQyLjE0OTY2MzksMjMuNzEyMDM4OSBDNDEuODgxMTU0OCwyMy43ODY4MDAyIDQxLjYwODk2MDMsMjMuODU4OTI5MSA0MS4zNDQ2NjMxLDIzLjkzNzM3NTkgQzM5LjIyNjYwMDEsMjQuNTU4MTA1OCAzNy4yOTI4MDgxLDI1LjMzNzMwODcgMzUuNDE4NTA5MywyNi4xMzM4ODU3IEMzMy41NDg0MjIzLDI2LjkzODg4NjUgMzEuNzUyMDQzOCwyNy44MDEyNzQ2IDI5Ljk4Nzc4MSwyOC42NzM2NjU5IEMyNi40NjM0Njc0LDMwLjQyNjM0NTkgMjMuMDgxODMyMiwzMi4yNzY5NTI4IDE5LjczOTE1NzEsMzQuMTExNzY1IEMxOC4wNzU5ODAyLDM1LjA0MzEyMyAxNi40MTY0ODg2LDM1Ljk1NDQ3NDUgMTQuNzcxMjEyMiwzNi44NjIxNDA2IEMxMy44NDAzODA3LDM3LjM3MjgzNDMgMTIuOTEyNzA4MSwzNy44NzgyNjMyIDExLjk4NzE0MTQsMzguMzczNjg4OSBDNy44OTM2OTM3OCw0MC42MzE3OTc4IDEuNTQ3NDAyMTYsNDQuNTA5OTExNiAwLjYwNzA5MzgxNyw0NS4wMTAwNzU2IEMtMC4yMDMxNzE4ODgsNDUuNDQwMjE2NyAtMC4wMDYyNjUyMTI0LDQ1LjgwOTgxMTUgMC4xMDMyNDQzODIsNDUuOTIyNDgwMSBDMC4yMDE2OTc3Miw0Ni4wMTYxOTUgMC45NTgyNjE2MDQsNDYuMzM3MzUzIDEuNDAyMDkxMzYsNDYuNDcwNTU0NSBDMS45NjM4NTQ1Miw0Ni42Mzk1NTczIDIuNTIxOTMyMjYsNDYuNzM2OTU3NyAzLjA4MDAxLDQ2Ljc5Njk3NzQgQzQuMTkwMzc0MTIsNDYuOTExMjI1NCA1LjI3NjUxOTc2LDQ2Ljg2NTk0NzQgNi4zMTI2NDkwMSw0Ni43MjQzMjIgQzYuODMxMjQwMTIsNDYuNjUwMDg3MSA3LjM0NDAzOTg2LDQ2LjU2Mzc0MyA3Ljg0MzY3NzM4LDQ2LjQ1MDU0OCBDOC4zNDY0NzM4NCw0Ni4zMzk0NTg5IDguODM2NjM0NTcsNDYuMjA3MzEwMyA5LjMyNDY4OTM2LDQ2LjA3NDYzNTIgQzEwLjMwMDI3MjQsNDUuODA3MTc5MSAxMS4yNTk1MzQ0LDQ1LjUxOTE4OTkgMTIuMjAxOTQ4Nyw0NS4yMDc1MDg4IEMxNS45NzczOTcyLDQzLjk3MTg0MDQgMTkuNTU0ODg2Miw0Mi40NzcxMzk3IDIzLjA4NzA5NzEsNDAuOTk1NjAxMyBDMjYuNjA2NjcyMywzOS40OTUxMDkyIDMwLjA3MzU5ODcsMzcuOTc3MjQzMSAzMy41MjIwOTc5LDM2LjYzMDQ4NTYgQzM1LjI0MzcxNTEsMzUuOTUyODk1IDM2Ljk2NjkxMTgsMzUuMzM4NDgzIDM4LjY2NzQ2OTQsMzQuNzk5ODg1NCBDMzkuNTE3MjIxNywzNC41Mjg3NDM4IDQwLjM2MzI4ODcsMzQuMjg3MDg1NiA0MS4xOTg4MjU4LDM0LjA4MDcwMjEgQzQyLjAzMzMxLDMzLjg3MjIxMjcgNDIuODU1Njg0OSwzMy42OTQ3ODYxIDQzLjY1MDY4MjUsMzMuNTYxMDU4MSBDNDMuODUwMjIxNiwzMy41MjU3ODMzIDQ0LjA0NDQ5NTgsMzMuNDk5NDU4OSA0NC4yNDA4NzYsMzMuNDY4Mzk2MSBMNDQuODE4NDMzOCwzMy4zOTYyNjcyIEw0NS4xMDAxMDUxLDMzLjM3MjU3NTIgTDQ1LjI0MDY3NzUsMzMuMzYwOTkyNSBDNDUuMjg1OTU1NiwzMy4zNTczMDcgNDUuMzQxMjM2OCwzMy4zNTA5ODkyIDQ1LjM1NzU1OCwzMy4zNTM2MjE2IEM0NS40NjIzMjkyLDMzLjM1ODM2IDQ1LjU0ODY3MzMsMzMuMzQ3MzAzOCA0NS42NzI5MjQ2LDMzLjM1OTQxMyBDNDYuMTMwOTY5NSwzMy4zNzM2MjgyIDQ2LjcyNzQ4MDksMzMuNDcyMDgxNSA0Ny4zNzgyMjA2LDMzLjYyNzkyMjEgQzQ4LjY5MTI4MjgsMzMuOTU5MDgzMyA1MC4xNzgwODYxLDM0LjU1NDU0MTcgNTEuNjcyMjYwMywzNS4yNTM3MTg0IEM1My4xNjg1NDA0LDM1Ljk2MjM3MTggNTQuNzAxMTQ4MywzNi43NTI2MzEgNTYuMjQ5MDI0MywzNy41NzgxNjQ4IEM1Ny4wMjM0ODg4LDM3Ljk4OTM1MjMgNTcuODAzNzQ0NiwzOC40MDc5MTA2IDU4LjU4NjEwNjQsMzguODM1OTQ1NyBDNTkuMzcwMDQ3NywzOS4yNjEzNDg0IDYwLjE1NTU2ODUsMzkuNjkzNTk1NCA2MC45Mzc0MDM4LDQwLjE0NDc5NiBDNjQuMDcyNjQyNCw0MS45MzI3NTA3IDcwLjcyNTM1MDMsNDYuMDk0MTE1MyA3MC43MjUzNTAzLDQ2LjA5NDExNTMgQzc2Ljk2NzM5NzIsNDkuMzc4MzUwMiA4NC4zNDg3NjUxLDQ2LjMwNTIzNzIgODYuNDIxNTUwMSw0NS4zMTM4NTk0IEM4Ni40MzUyMzg4LDQ1LjMwNDM4MjYgODYuNzE5NTQyNiw0NS4xNjU5MTYyIDg2LjcyNjkxMzQsNDQuODU2MzQxIEM4Ni43MjY5MTM0LDQ0Ljc0NjgzMTQgODYuNjkwMDU5Miw0NC42NDYyNzIxIDg2LjYzMDAzOTUsNDQuNTY0NjY2NCIgaWQ9IkZpbGwtNCI+PC9wYXRoPiAgICAgICAgICAgICAgICAgICAgPC9nPiAgICAgICAgICAgICAgICAgICAgPGcgaWQ9Ikdyb3VwIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjAwMDAwMCwgNjguMDkwOTA5KSI+ICAgICAgICAgICAgICAgICAgICAgICAgPHBvbHlnb24gaWQ9IkZpbGwtNyIgcG9pbnRzPSIxMy40Njg3MTQ5IDE2LjkwMTU3MTEgMTAuOTUxNTczNyAxNi45MDE1NzExIDYuNzkyODQxNTYgMTAuMzE1MjAwOCAyLjYzNDYzNTg5IDE2LjkwMTU3MTEgMC4xMzk2MDcxODQgMTYuOTAxNTcxMSA1LjUzNDAwNzcxIDguNTg0NjMzMjggMC43MjQwMDkzNDkgMS40MTQzODcyNSAzLjI0MTE1MDU3IDEuNDE0Mzg3MjUgNi43OTI4NDE1NiA2Ljg3NjE3ODMgMTAuMzY2NjQ1MSAxLjQxNDM4NzI1IDEyLjg4NDMxMjggMS40MTQzODcyNSA4LjA1MTY3NTQxIDguNTg0NjMzMjgiPjwvcG9seWdvbj4gICAgICAgICAgICAgICAgICAgICAgICA8cG9seWdvbiBpZD0iRmlsbC05IiBwb2ludHM9IjQyLjE3MzY1NDMgMS40MTQzODcyNSAzNi45MzYxNDczIDE2LjkwMTU3MTEgMzUuMTE1NTUwMyAxNi45MDE1NzExIDMyLjM3MzA3MiA3LjIzNjI5NjM5IDI5LjY1MzIzMjcgMTYuOTAxNTcxMSAyNy44MzI2MzU3IDE2LjkwMTU3MTEgMjIuNTk1MTI4OCAxLjQxNDM4NzI1IDI0Ljg0MzIzNDQgMS40MTQzODcyNSAyOC43MzE4NzggMTMuNTI5OTM5MiAzMS41ODY0OTgzIDMuNTA1MDcyODQgMzMuMTgyMjg0NyAzLjUwNTA3Mjg0IDM2LjAzNzQzMTUgMTMuNTI5OTM5MiAzOS45MjU1NDg2IDEuNDE0Mzg3MjUiPjwvcG9seWdvbj4gICAgICAgICAgICAgICAgICAgICAgICA8cG9seWdvbiBpZD0iRmlsbC0xMSIgcG9pbnRzPSI1My4xNjY3MzI4IDE2LjkwMTQxMzIgNTUuMjU2ODkxOSAxNi45MDE0MTMyIDU1LjI1Njg5MTkgMS40MTQyMjkzMSA1My4xNjY3MzI4IDEuNDE0MjI5MzEiPjwvcG9seWdvbj4gICAgICAgICAgICAgICAgICAgICAgICA8cG9seWdvbiBpZD0iRmlsbC0xMyIgcG9pbnRzPSI3Ny4xNzMyODkzIDE2LjkwMTU3MTEgNzAuMTE1MTg1MyA1LjIzNTY0MDMzIDcwLjExNTE4NTMgMTYuOTAxNTcxMSA2OC4wMjUwMjYyIDE2LjkwMTU3MTEgNjguMDI1MDI2MiAxLjQxNDM4NzI1IDcwLjA5MzA3MjggMS40MTQzODcyNSA3Ny4wODMyNTk4IDEyLjk5MDI4ODUgNzcuMDgzMjU5OCAxLjQxNDM4NzI1IDc5LjE3Mzk0NTQgMS40MTQzODcyNSA3OS4xNzM5NDU0IDE2LjkwMTU3MTEiPjwvcG9seWdvbj4gICAgICAgICAgICAgICAgICAgICAgICA8cGF0aCBkPSJNMTAyLjQ4NDMyNiw0LjY3MzU2MTI3IEMxMDEuNDk1MDU0LDMuNzk2OTU4MDMgMTAwLjE5MTQ2OSwzLjIzNTE5NDg2IDk4Ljc1MjU3NjIsMy4yMzUxOTQ4NiBDOTUuNTE2MjUxOCwzLjIzNTE5NDg2IDkzLjAyMDY5NjYsNS45MTAyODI2MSA5My4wMjA2OTY2LDkuMTkxMzU4NTUgQzkzLjAyMDY5NjYsMTIuNDcyOTYxIDk1LjUxNjI1MTgsMTUuMTI1OTM2MiA5OC43NTI1NzYyLDE1LjEyNTkzNjIgQzEwMS41Mzk4MDYsMTUuMTI1OTM2MiAxMDMuNTYzMTAxLDEzLjUwNjk4NDMgMTAzLjY5Nzg4MiwxMC45NDUwOTE1IEw5OS4yOTI3NTMzLDEwLjk0NTA5MTUgTDk5LjI5Mjc1MzMsOS4xMjM5NjgwMyBMMTA2LjA4MDc2OSw5LjEyMzk2ODAzIEMxMDYuMDgwNzY5LDE0LjU2MzY0NjYgMTAzLjE4MDg3LDE3LjEyNjA2NTggOTguNzUyNTc2MiwxNy4xMjYwNjU4IEM5NC4zNDY5MjA5LDE3LjEyNjA2NTggOTAuOTMwNTM3NSwxMy41NzQ5MDEzIDkwLjkzMDUzNzUsOS4xOTEzNTg1NSBDOTAuOTMwNTM3NSw0Ljc4NjIyOTggOTQuMzQ2OTIwOSwxLjIzNDUzODggOTguNzUyNTc2MiwxLjIzNDUzODggQzEwMC43OTg1MSwxLjIzNDUzODggMTAyLjY0MTc0NiwyLjAyMTExMjUzIDEwMy45OTAwODMsMy4yNzk5NDYzOCBMMTAyLjQ4NDMyNiw0LjY3MzU2MTI3IFoiIGlkPSJGaWxsLTE0Ij48L3BhdGg+ICAgICAgICAgICAgICAgICAgICA8L2c+ICAgICAgICAgICAgICAgIDwvZz4gICAgICAgICAgICA8L2c+ICAgICAgICA8L2c+ICAgIDwvZz48L3N2Zz4=", "26.11111111");
}

function codex_custom_init() {
	//--------------------------------------TEAM------------------------------------------
	$labels = array(
		'name'               => _x( 'Team', 'post type general name', 'xwing' ),
		'singular_name'      => _x( 'Member', 'post type singular name', 'xwing' ),
		'menu_name'          => _x( 'Team', 'admin menu', 'xwing' ),
		'name_admin_bar'     => _x( 'Team', 'add new on admin bar', 'xwing' ),
		'add_new'            => _x( 'Add new', 'add new member', 'xwing' ),
		'add_new_item'       => __( 'Add new member', 'xwing' ),
		'new_item'           => __( 'New member', 'xwing' ),
		'edit_item'          => __( 'Edit member', 'xwing' ),
		'view_item'          => __( 'view member', 'xwing' ),
		'all_items'          => __( 'Team', 'xwing' ),
		'search_items'       => __( 'Search members', 'xwing' ),
		'parent_item_colon'  => __( 'Parent members:', 'xwing' ),
		'not_found'          => __( 'Members not found.', 'xwing' ),
		'not_found_in_trash' => __( 'Members not found in trash.', 'xwing' )
	);

	$args = array(
		'public' => true,
		'label' => 'Team',
		'supports' => array( 'title', 'editor', 'thumbnail'),
		'labels' => $labels,
		'menu_icon' => '',
		'show_in_menu' => 'xwing', // Поместить как подменю в уже существующее меню с соответствующим идентификатором
	);
	register_post_type( 'ca_team', $args );
	
	//--------------------------------------COMPANIES------------------------------------------
	$labels = array(
		'name'               => _x( 'Companies', 'post type general name', 'xwing' ),
		'singular_name'      => _x( 'Comapny', 'post type singular name', 'xwing' ),
		'menu_name'          => _x( 'Companies', 'admin menu', 'xwing' ),
		'name_admin_bar'     => _x( 'Companies', 'add new on admin bar', 'xwing' ),
		'add_new'            => _x( 'Add new', 'add new company', 'xwing' ),
		'add_new_item'       => __( 'Add new company', 'xwing' ),
		'new_item'           => __( 'New company', 'xwing' ),
		'edit_item'          => __( 'Edit company', 'xwing' ),
		'view_item'          => __( 'view company', 'xwing' ),
		'all_items'          => __( 'Companies', 'xwing' ),
		'search_items'       => __( 'Search company', 'xwing' ),
		'parent_item_colon'  => __( 'Parent company:', 'xwing' ),
		'not_found'          => __( 'Companies not found.', 'xwing' ),
		'not_found_in_trash' => __( 'Companies not found in trash.', 'xwing' )
	);

	$args = array(
		'public' => true,
		'label' => 'Companies',
		'supports' => array( 'title', 'thumbnail'),
		'labels' => $labels,
		'menu_icon' => '',
		'show_in_menu' => 'xwing', // Поместить как подменю в уже существующее меню с соответствующим идентификатором
	);
	register_post_type( 'ca_company', $args );
	
	//--------------------------------------INVESTORS------------------------------------------
	$labels = array(
		'name'               => _x( 'Investors', 'post type general name', 'xwing' ),
		'singular_name'      => _x( 'Investor', 'post type singular name', 'xwing' ),
		'menu_name'          => _x( 'Investors', 'admin menu', 'xwing' ),
		'name_admin_bar'     => _x( 'Investors', 'add new on admin bar', 'xwing' ),
		'add_new'            => _x( 'Add new', 'add new investor', 'xwing' ),
		'add_new_item'       => __( 'Add new investor', 'xwing' ),
		'new_item'           => __( 'New investor', 'xwing' ),
		'edit_item'          => __( 'Edit investor', 'xwing' ),
		'view_item'          => __( 'view investor', 'xwing' ),
		'all_items'          => __( 'Investors', 'xwing' ),
		'search_items'       => __( 'Search investor', 'xwing' ),
		'parent_item_colon'  => __( 'Parent investor:', 'xwing' ),
		'not_found'          => __( 'Investors not found.', 'xwing' ),
		'not_found_in_trash' => __( 'Investors not found in trash.', 'xwing' )
	);

	$args = array(
		'public' => true,
		'label' => 'Investors',
		'supports' => array( 'title'),
		'labels' => $labels,
		'menu_icon' => '',
		'show_in_menu' => 'xwing', // Поместить как подменю в уже существующее меню с соответствующим идентификатором
	);
	register_post_type( 'ca_investor', $args );
}
add_action( 'init', 'codex_custom_init' );

?>