<?php
	global $ca_post_type;
	$ca_post_type = 'ca_investor';

	add_action('add_meta_boxes', 'ca_xwing_position_investors');
	function ca_xwing_position_investors($post_type) {
		global $ca_post_type;

		if ($post_type == $ca_post_type) {

			add_meta_box(
				'ca_xwing-position-meta-box', // HTML 'id' attribute of the edit screen section.
				__('Position, Company'),              // Title of the edit screen section, visible to user.
				'ca_xwing_investors_meta_box', // Function that prints out the HTML for the edit screen section.
				$post_type,          // The type of Write screen on which to show the edit screen section.
				'advanced',          // The part of the page where the edit screen section should be shown.  'normal', 'side', and 'advanced'
				'high'               // The priority within the context where the boxes should show. 'high', 'low'
			);
		}

	}

	function ca_xwing_investors_meta_box($post, $metabox) {
		$position = get_post_meta($post->ID, '_ca_xwing_position', true);

		?>
			<textarea style="width: 100%; height:; 30px;" id="ca_xwing_position" name="ca_xwing_position" placeholder="Position, Company"><?php echo esc_attr($position); ?></textarea>
		<?php

		wp_nonce_field(
			plugin_basename(__FILE__), // Action name.
			'ca_xwing_position_meta_box' // Nonce name.
		);
	}

	add_action('save_post', 'ca_xwing_investors_position_save_postdata');
	function ca_xwing_investors_position_save_postdata($post_id) {
		global $ca_post_type;
		if ((($_POST['post_type'] == $ca_post_type) && current_user_can('edit_page', $post_id) || current_user_can('edit_post', $post_id))) {
		
			if ((( ! defined('DOING_AUTOSAVE')) || ( ! DOING_AUTOSAVE)) && (( ! defined('DOING_AJAX')) || ( ! DOING_AJAX))) {

				if (wp_verify_nonce($_POST['ca_xwing_position_meta_box'], plugin_basename(__FILE__))) {
					$position = ($_POST['ca_xwing_position']);

					if ($position !== '') add_post_meta($post_id, '_ca_xwing_position', $position, true) OR update_post_meta($post_id, '_ca_xwing_position', $position);
					else delete_post_meta($post_id, '_ca_xwing_position');
				}

			}

		}

	}
?>