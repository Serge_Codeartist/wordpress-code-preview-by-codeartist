<?php
	function ca_the_meta($name, $id = 0) {
		$id = $id !== 0 ? $id : get_the_ID();
		echo get_post_meta($id, $name, true);
	}

	function ca_get_meta($name, $id = 0) {
		$id = $id !== 0 ? $id : get_the_ID();
		return get_post_meta($id, $name, true);
	}

	add_action('edit_form_after_title', 'xwing_move_subtitle');
	function xwing_move_subtitle() {
		global $post, $wp_meta_boxes, $ca_post_types;
		$ca_post_types = array(
			'ca_team',
			'ca_investor',
		);
		do_meta_boxes(get_current_screen(), 'advanced', $post);
		foreach($ca_post_types as $pt)
			unset($wp_meta_boxes[$pt]['advanced']);
	}
?>