<?php
/**
 * Template for displaying the footer
 *
 */
$opt = get_option("codeartist_options");
?>
	<div class="ca_contact_form ca_hidden">
		<div class="container">
			<div class="row">
				<div class="col-12 ca_static">
					<a href="#" class="ca_close_form"></a>
				</div>
				<div class="col-12 col-md-5">
					<h2><?php echo $opt['ca_general_form_txt_1']; ?></h2>
					<h3><?php echo $opt['ca_general_form_txt_2']; ?></h3>
				</div>
				<div class="col-12 col-md-7">
					<?php echo do_shortcode('[contact-form-7 id="78" title="Contact Form"]'); ?>
				</div>
			</div>
		</div>
	</div>
	<footer class="ca_footer">
		<div class="container">
			<div class="row">
				<div class="col-md-1 ca_one"></div>
				<div class="col-md-3 ca_two">
					<a href="/" class="ca_logo"><img src="<?php echo get_template_directory_uri().'/'; ?>img/logo_v.svg" alt="XWing"></a>
				</div>
				<nav class="col-6 col-md-2 ca_three">
					<?php wp_nav_menu( array( 'theme_location' => 'foot' ) ); ?>
				</nav>
				<div class="col-md-2 ca_five">
					<h3>Contact us</h3>
					<p><?php echo str_replace("\n", '<br/>', $opt['ca_general_address']); ?></p>
					<p><?php echo $opt['ca_general_phone']; ?></p>
				</div>
				<div class="col-6 col-md-2 ca_four">
					<h3>Follow us</h3>
					<a href="<?php echo $opt['ca_general_tw']; ?>" class="ca_social" target=_blank><span class="fab fa-twitter"></span></a>
					<a href="<?php echo $opt['ca_general_in']; ?>" class="ca_social" target=_blank><span class="fab fa-linkedin"></span></a>
				</div>
				<div class="col-12 ca_six">
					<p class="ca_copyright">© 2018 - All rights reserved</p>
				</div>
			</div>
		</div>
	</footer>

<?php wp_footer(); ?>

</body>
</html>