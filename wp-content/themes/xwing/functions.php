<?php
/**
 * Theme functions and definitions
 */

add_action( 'after_setup_theme', 'xwing_setup' );

if ( ! function_exists( 'xwing_setup' ) ):

function xwing_setup() {
	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', __( 'Primary Menu', 'xwing' ) );
	register_nav_menu( 'foot', __( 'Footer Menu', 'xwing' ) );
	// This theme uses Featured Images (also known as post thumbnails) for per-post/per-page Custom Header images
	add_theme_support( 'post-thumbnails' );
}
endif; // xwing_setup

add_filter('upload_mimes', 'ca_mime_types');
function ca_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}

require_once('includes/codeartist_customize.php');
require_once('includes/cpt.php');
require_once('includes/metaboxes.php');
require_once('includes/metaboxes-team.php');
require_once('includes/metaboxes-investors.php');