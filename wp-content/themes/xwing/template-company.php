<?php
/**
 * Template Name: Company Template
 *
 */
$opt = get_option("codeartist_options");
get_header(); ?>
	<section class="ca_company_top">
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<h1><?php echo $opt['ca_company_1_title']; ?></h1>
					<img src="<?php echo get_template_directory_uri().'/'; ?>img/company_top_bg.jpg" alt="XWing">
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
		<div class="container ca_company_info">
			<div class="row">
				<div class="col-12">
					<h2><?php echo $opt['ca_company_2_title']; ?></h2>
				</div>
				<div class="col-md-2 col-12"></div>
				<div class="col-md-8 col-12">
					<h3>“<?php echo $opt['ca_company_2_txt_quote']; ?>”</h3>
					<h4><?php echo $opt['ca_company_2_txt_quote_author']; ?></h4>
					<?php echo $opt['ca_company_2_txt']; ?>
					<div class="ca_arrow"></div>
				</div>
				<div class="col-md-2 col-12"></div>
			</div>
		</div>
	</section>
	<section class="ca_comapny_members">
		<div class="container">
			<?php
				$qry = new WP_Query(array(
					'post_type' => 'ca_team',
					'posts_per_page' => -1,
				));
				$num = 0;
				while($qry->have_posts()): $qry->the_post();
					$num++;
			?>
					<div class="row">
						<?php if($num == 1) : ?>
								<div class="col-12">
									<h2><?php echo $opt['ca_company_3_title']; ?></h2>
								</div>
						<?php endif; ?>
						<div class="col-md-1"></div>
						<div class="col-md-5">
							<h3><?php the_title() ?></h3>
							<h4><?php ca_the_meta('_ca_xwing_position'); ?></h4>
							<?php the_content(); ?>
						</div>
						<div class="col-md-3">
							<?php
								if(has_post_thumbnail())
								{
									$thumb_id = get_post_thumbnail_id();
									$thumb_url = wp_get_attachment_image_src($thumb_id,'full', true);
									?>
										<img src="<?php echo $thumb_url[0]; ?>" alt="<?php the_title(); ?>">
									<?php
								}
							?>
						</div>
						<div class="col-md-3"></div>
					</div>
			<?php
				endwhile;
			?>
		</div>
	</section>
	<section class="ca_company_investors">
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-3">
					<h2><?php echo $opt['ca_company_4_title']; ?></h2>
					<h3><?php echo $opt['ca_company_4_subtitle']; ?></h3>
				</div>
				<div class="col-md-8">
					<div class="ca_slider_one">
					<?php
						$qry = new WP_Query(array(
							'post_type' => 'ca_company',
							'posts_per_page' => -1,
						));
						while($qry->have_posts()) {
							$qry->the_post();
							if(has_post_thumbnail())
							{
								$thumb_id = get_post_thumbnail_id();
								$thumb_url = wp_get_attachment_image_src($thumb_id,'full', true);
								?>
									<img src="<?php echo $thumb_url[0]; ?>" alt="<?php the_title(); ?>">
								<?php
							}
						}
					?>
					</div>
					<div class="ca_slider_one_nav"></div>
					<div class="ca_slider_two">
					<?php
						$qry = new WP_Query(array(
							'post_type' => 'ca_investor',
							'posts_per_page' => -1,
						));
						while($qry->have_posts()) :
							$qry->the_post();
							?>
								<div class="ca_item">
									<h3><?php the_title(); ?></h3>
									<p><?php echo implode("</br>", explode("\n",ca_get_meta('_ca_xwing_position'))); ?></p>
								</div>
							<?php
						endwhile;
					?>
					</div>
					<div class="ca_slider_two_nav"></div>
					<!-- <p><a href="<?php echo $opt['ca_company_4_btn_lnk']; ?>"><?php echo $opt['ca_company_4_btn_txt']; ?></a></p> -->
					<p><?php echo $opt['ca_company_4_btn_txt']; ?></p>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>