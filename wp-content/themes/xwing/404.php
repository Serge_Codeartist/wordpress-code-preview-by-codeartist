<?php
/**
 * Template for displaying 404 pages (Not Found)
 *
 */

wp_redirect( 'https://medium.com/xwing' ); exit; // Redirect all 404 to the blog

get_header(); ?>

	<section class="ca_404">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h1>Oops!</h1>
					<p>We can’t find what you’re looking for</p>
					<p><a href="/" class="ca_button">Back to Homepage</a></p>
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>